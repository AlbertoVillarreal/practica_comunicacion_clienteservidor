/*
 * terminal.c
 *
 *  Created on: 17/03/2017
 *      Author: Alberto
 */

#include "terminal.h"

#define ECHO_BUFFER_LENGTH 8

//uart_config_t uartConfig;
uart_transfer_t transfer;

#define DEMO_UART UART0
#define DEMO_UART_CLKSRC UART0_CLK_SRC
#define DEMO_UART_RX_TX_IRQn UART0_RX_TX_IRQn
uint8_t background_buffer[32];
uint8_t recv_buffer[4];
uart_rtos_handle_t handle;
struct _uart_handle t_handle;


uart_transfer_t sendXfer;
uart_transfer_t receiveXfer;

//NUEVO
uint8_t numMenu[1];
typedef enum{
	MENU1 = 0X31,
	MENU2 = 0X32,
	MENU3 = 0X33,
	MENU4 = 0X34,
	MENU5 = 0X35,
	MENU6 = 0X36,
	MENU7 = 0X37,
	MENU8 = 0X38,
	MENU9 = 0X39
}menus;



/////////////////////////////7

const char *leerMemoria = "\033[2J\033[1;1HDireccion de lectura";


uint8_t dir[] = "0x0000";


volatile uint8_t selector = 0;
volatile bool direccion = false;
volatile bool longitudB = false;
volatile uint8_t cont = 0;


bool leerMemoriaFlag = false;
bool escribirMemoriaFlag = false;
bool establecerHoraFlag = false;
bool establecerFechaFlag = false;
bool formatoHoraFlag = false;



/*Funci�n para comunicarse con todos los usuarios conectados al chat grupal o chat de dos personas
 * se tienen una serie de condicionales para ver si se quiere escribir en el grupo o individual*/
void userCommunication(uint8_t id, struct netconn *conn)
{
	struct netbuf *buf;
	static struct netconn *conn2;
	uint16_t len;
	uint8_t i = 0;
	uint8_t x = 0;
	uint8_t usernameLenght = 0;
	uint8_t messageLenght = 0;
	void *data;
	conn2 = data_client_type[id].conn;
	uint8_t chat = 0;

	while(conn != data_client_type[i].conn)
	{
		i++;
	}

	netconn_write(conn,menu_userCom1,strlen(menu_userCom1), 0x0);
	netconn_write(conn,data_client_type[id].username,strlen(data_client_type[id].username), 0x0);
	netconn_write(conn,menu_userCom2,strlen(menu_userCom2), 0x0);

	netconn_recv(conn, &buf); //Esta esperandoa a que le llegue un mensaje
	netbuf_data(buf, &data, &len);
	char * data2 = strcat(data,"New");
	while (netbuf_next(buf) >= 0);
	netbuf_delete(buf);
	x = 0;
	while('\r' != data2[x])
	{
		data_client_type[i].username[x] = data2[x];
		x++;
	}
	usernameLenght = x + BIT1;
	data_client_type[i].username[x] = ':';
	netconn_write(data_client_type[i].conn,menu_userCom3,strlen(menu_userCom3), 0x0);

	netconn_recv(conn, &buf); //Esta esperandoa a que le llegue un mensaje
	netbuf_data(buf, &data, &len);
	char* data3 = strcat(data,"New");
	while (netbuf_next(buf) >= 0);
	netbuf_delete(buf);

	if(YES == data3[BIT0])
	{
		data_client_type[id].chatFlag = pdTRUE;

		while('*' != data2[0])
		{
			netconn_recv(conn, &buf); //Esta esperandoa a que le llegue un mensaje
			netbuf_data(buf, &data, &len);
			data2 = strcat(data,"New");
			netbuf_delete(buf);
			x = 0;
			while('\r' != data2[x])
			{
				data_client_type[i].datos[x] = data2[x];
				x++;
			}
			messageLenght = x + BIT1;
			data_client_type[i].datos[x] = '\n';
			for(x = 0; x<4; x++)
			{
				if(pdTRUE == data_client_type[x].chatFlag)
				{
					if(data_client_type[id].conn == data_client_type[x].conn)
					{

					}
					else
					{
						netconn_write(data_client_type[x].conn,data_client_type[id].username,usernameLenght, 0x0);
						netconn_write(data_client_type[x].conn,data_client_type[id].datos,messageLenght, 0x0);

					}
				}
			}
		}
		data_client_type[id].chatFlag = pdTRUE;
	}
	else
	{
		data_client_type[id].privateFlag = pdTRUE;
		while('*' != data2[0])
		{
			if(pdTRUE == data_client_type[id].chat)
			{
				netconn_recv(conn, &buf); //Esta esperandoa a que le llegue un mensaje
				netbuf_data(buf, &data, &len);
				data2 = strcat(data,"New");
				netbuf_delete(buf);
				x = 0;
				while('\r' != data2[x])
				{
					data_client_type[i].datos[x] = data2[x];
					x++;
				}
				messageLenght = x + BIT1;
				data_client_type[i].datos[x] = '\n';

				if(data_client_type[data_client_type[id].numChat].numChat == data_client_type[id].id)
				{
					netconn_write(data_client_type[data_client_type[id].numChat].conn,data_client_type[id].username,usernameLenght, 0x0);
					netconn_write(data_client_type[data_client_type[id].numChat].conn,data_client_type[id].datos,messageLenght, 0x0);

				}
			}
			else
			{
				for(x = 0; x<4; x++)
				{
					if(pdTRUE == data_client_type[x].privateFlag)
					{
						if(data_client_type[id].conn == data_client_type[x].conn)
						{

						}
						else
						{
							data_client_type[x].username[usernameLenght] = '\n';
							netconn_write(data_client_type[id].conn,data_client_type[x].username,usernameLenght, 0x0);

							if( data2[0] == data_client_type[x].username[0] )
							{
								data_client_type[id].numChat = data_client_type[x].id;
								data_client_type[id].chat = pdTRUE;

							}
						}

					}

				}

				netconn_write(data_client_type[id].conn,chatP2,strlen(chatP2), 0x0);
				netconn_recv(conn, &buf); //Esta esperandoa a que le llegue un mensaje
				netbuf_data(buf, &data, &len);
				data2 = strcat(data,"New");
				netbuf_delete(buf);
			}
		}
	}

}

stateServerDefinitionType listen(void * apuntador, uint8_t communicationType, uint8_t id)
{
	uint16_t menu;
	uint8_t i = 0;
	char *data2;
	struct netconn *conn;
	/*
	 * Condicional para ver si es UART o TCP
	 */
	if(communicationType)
	{
		conn= apuntador;
		write(conn, communicationType, mensaje_menu1, id);

		data2 = read(conn, communicationType, id);

	}
	else
	{
		conn= apuntador;
		write(conn, communicationType, mensaje, id);

		data2 = read(conn, communicationType, id);
	}
	/*while en el cual se busca una posici�n vac�a en el arreglo para luego agregarle el cliente en el que se esta*/

	while(NULL != data_client_type[i].conn)
	{
		i++;
	}

	data_client_type[i].conn = conn;//Se le asigna el cliente actual.
	data_client_type[i].id = i;

	/*Se bsuca a que menu se accedio para utilizar las funciones correspondientes.*/
	switch(data2[BIT0])
	{
	case MENU1:
		menu = state1;
		readMemory(conn,communicationType, id);
		break;
	case MENU2:
		menu = state2;
		writeMemory(conn,communicationType, id);
		break;
	case MENU3:
		menu = state3;
		setUpHour(conn,communicationType, id);
		break;
	case MENU4:
		menu = state4;
		setUpDate(conn,communicationType, id);
		break;
	case MENU5:
		menu = state5;
		formatHours(conn,communicationType, id);
		break;
	case MENU6:
		menu = state6;
		readHour(conn,communicationType, id);
		break;
	case MENU7:
		menu = state7;
		readDate(conn,communicationType, id);
		break;
	case MENU8:
		menu = state8;
		if(communicationType)
		{
			userCommunication(i, conn);
		}
		else
		{
			terminalCom(id);
		}
		break;
	case MENU9:
		menu = state9;
		echoLCD(conn,communicationType, id);
		break;
	default: menu = state9;
	}
	return menu;
}

/*En esta funci�n se va a mandar a escribir el menu */


//////////////////////////////




