/*
 * UART.h
 *
 *  Created on: 14/05/2017
 *      Author: Alberto
 */

#ifndef DRIVERS_PRACTICA1_UART_H_
#define DRIVERS_PRACTICA1_UART_H_

#include "fsl_uart.h"
#include "fsl_port.h"
#include "fsl_gpio.h"
#include "board.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "General_functions.h"
#include "FreeRTOS.h"

/*
 * Variables para la inicialización de la UART
 */
uart_config_t config;
uart_config_t uart4Config;
uart_transfer_t xfer;
uart_transfer_t transfer4;
uart_transfer_t sendXfer;
uart_transfer_t receiveXfer;
uart_transfer_t sendXfer4;
uart_transfer_t receiveXfer4;
#define DEMO_UART UART0
#define DEMO_UART_CLKSRC UART0_CLK_SRC
#define ECHO_BUFFER_LENGTH 1
#define BUFFER_LENGTH 20
#define SALIDA			'*'


uint8_t g_txBuffer[ECHO_BUFFER_LENGTH] = {0};
uint8_t g_rxBuffer[ECHO_BUFFER_LENGTH] = {0};
uint8_t g_tx4Buffer[ECHO_BUFFER_LENGTH] = {0};
uint8_t g_rx4Buffer[ECHO_BUFFER_LENGTH] = {0};
uint8_t reciveBuffer[BUFFER_LENGTH] = {0};
uint8_t recive4Buffer[BUFFER_LENGTH] = {0};
volatile bool rxBufferEmpty = true;
volatile bool txBufferFull = false;
volatile bool rx4BufferEmpty = true;
volatile bool tx4BufferFull = false;
volatile bool txOnGoing = false;
volatile bool rxOnGoing = false;
volatile bool tx4OnGoing = false;
volatile bool rx4OnGoing = false;
uart_handle_t g_uartHandle;
uart_handle_t g_uart4Handle;

char* mensajeChat = "Estas en el chat de la terminal";
char* mensajenext = "\n";
const char *erase = "\033[2J\033[1;1H";

typedef enum
{
	bluetooth,
	teraterm
}identifier;

typedef struct
{
	UART_Type * uart;
	bool chatFlag;
}uartStruct;

/* Funciones de la UART */
void terminalInit();
void terminalBluetoothInit();
void UART_UserCallback(UART_Type *base, uart_handle_t *handle, status_t status, void *userData);
void UART4_UserCallback(UART_Type *base, uart_handle_t *handle, status_t status, void *userData);

void uartSend(char * mensaje, uint8_t id);
char  *uartR(uint8_t id);

void terminalCom(uint8_t id);

#endif /* DRIVERS_PRACTICA1_UART_H_ */
