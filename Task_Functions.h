/*
 * Task_Functions.h
 *
 *  Created on: 12/04/2017
 *      Author: Salvador
 */

#ifndef SOURCE_TASK_FUNCTIONS_H_
#define SOURCE_TASK_FUNCTIONS_H_

#include <string.h>
#include "FreeRTOS.h"
#include "queue.h"
#include "RTC_and_EEPROM_Functions.h"
#include "SPI_functions.h"
#include "General_functions.h"
#include "fsl_uart_freertos.h"
#include "fsl_uart.h"

#include "api.h"
#include "projdefs.h"
#include "abstractionL.h"

#define ENTER 			0x0D
#define FORMATBIT1 		0x40
#define FORMATBIT0		0x00
#define FORMATMASK		0x1F
#define OVERFLOW_HOUR	0x13
#define MAX_HOUR		0x12
#define AMPMBIT			0x20
#define YES				0x31
#define NO				0x30
#define ASCII_NUMBER	0x30
#define ASCII_LETTER	0x37
#define BYTE_LENGTH		0x08
#define HIGH_BYTE		0xF0
#define LOW_BYTE		0x0F
#define SALIDA 			'*'
#define FINAL			'\r'
/*
 * Variables para la inicialización de la UART
 */
uart_config_t config;
uart_transfer_t xfer;
uart_transfer_t sendXfer;
uart_transfer_t receiveXfer;
#define DEMO_UART UART0
#define DEMO_UART_CLKSRC UART0_CLK_SRC

/*
 * Variables para las funciones de las opciones del menu
 */
static uint8_t formatHour;
static uint8_t am_pm;
static uint8_t hours;
static uint8_t minutes;
static uint8_t seconds;
static uint8_t day;
static uint8_t month;
static uint8_t year;

/*Se crea una estructura para hacer un arreglo que contenga información importante que permita
 * la comunicación entre dos terminales*/
typedef struct prueba{
	struct netconn *conn;
	char username[10];
	bool chatFlag;
	bool privateFlag;
	uint8_t id;
	char datos[20];
	uint8_t numChat;
	bool chat;
}data_client;
/*Arreglo con la información necesaria para crear un chat grupal o individual*/
static data_client data_client_type[4] ={
		{NULL, "Cliente1", pdFALSE, pdFALSE, 0, "   ",0, pdFALSE},
		{NULL, "Cliente2", pdFALSE, pdFALSE, 0, "   ",0, pdFALSE},
		{NULL, "Cliente3", pdFALSE, pdFALSE, 0, "   ",0, pdFALSE},
		{NULL, "Cliente4", pdFALSE, pdFALSE, 0, "   ",0, pdFALSE},
};


/*Textos para el menu de leer memoria*/
const char *completed_readMem = "Presiona la tecla ESC para continuar...";
const char *menu_readMem1 = "Direccion de lectura:";
const char *menu_readMem2 = "\nLongitud en bytes:";
const char *menu_readMem3 = "\nContenido:";

/*Textos para el menu de escribir memoria*/
const char *menu_writeMem1 = "\nDireccion de escritura:";
const char *menu_writeMem2 = "\nTexto a guardar:";
const char *menu_writeMem3 = "\nSu texto ha sido guardado...";

/*Textos para el menu de escribir la hora*/
const char *menu_setHour = "Escribir hora en hh:mm:ss: ";
const char *completed_setHour = "La hora ha sido cambiada... ";

/*Textos para el menu de escribir fecha*/
const char *menu_setDate = "Escribir fecha en dd/mm/aa: ";
const char *completed_setDate = "La fecha ha sido cambiada... ";

/*Texto para el menu de leer hora*/
const char *menu_getHour = "La hora actual es:";

/*Texto para el menu de leer fecha*/
const char *menu_getDate = "La fecha actual es:";

/*Textos para el menu de echo en la LCD*/
const char *menu_echoLCD = "Echo LCD ";

/*Textos para el menu de formato de hora*/
const char* menu_formatoHora12_1 = "El formato actual es 12: Desea cambiar el formato a 24h ";
const char* menu_formatoHora12_2 = 	"si/no (1/0)? ";
const char* menu_formatoHora24_1 = "El formato actual es 24: Desea cambiar el formato a 12h ";
const char* menu_formatoHora24_2 = 	"si/no (1/0)? ";
const char* completed_formatoHora = "El formato ha sido cambiado";

/*Textos para el menu de comunicación con otro usuario*/
const char* menu_userCom1 = "Usted esta en:";
const char* menu_userCom2 = "\nIngrese su usuario";
const char* menu_userCom3 = "Presione 1 para ingresar al chat grupal \npresione 2 para ingresar al chat privado.";

/*Textos para chat privado*/
const char* chatP1 = "Usiarios conectados:";
const char* chatP2 = "Refrescar Lista:";


uint8_t string1_LCD[]="Fecha: ";
uint8_t string2_LCD[]="Hora: ";

/*Funciones que realisan las actividades correspondientes al menu de la práctica*/
void readMemory(struct netconn *conn, uint8_t communicationType, uint8_t id);
void writeMemory(struct netconn *conn, uint8_t communicationType, uint8_t id);
void setUpHour(struct netconn *conn, uint8_t communicationType, uint8_t id);
void setUpDate(struct netconn *conn, uint8_t communicationType, uint8_t id);
void readHour(struct netconn *conn, uint8_t communicationType, uint8_t id);
void readDate(struct netconn *conn, uint8_t communicationType, uint8_t id);
void echoLCD(struct netconn *conn, uint8_t communicationType, uint8_t id);
void formatHours(struct netconn *conn, uint8_t communicationType, uint8_t id);

/*Función donde se hace el recive*/
char* readSocket(struct netconn *conn, struct netbuf *buf);
void LCD_Print();



#endif /* SOURCE_TASK_FUNCTIONS_H_ */
