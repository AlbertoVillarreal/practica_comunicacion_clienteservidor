/*
 * abstractionL.c
 *
 *  Created on: 13/05/2017
 *      Author: Alberto
 */

#include "abstractionL.h"

void write(void * apuntador, uint8_t communicationType, const char * mensaje, uint8_t id)
{
	struct netconn *conn;
	if(communicationType)
	{
		conn = apuntador;
		netconn_write(conn,mensaje,strlen(mensaje), BIT0);
	}
	else
	{
		uartSend(erase, id);
		uartSend(mensaje, id);
	}

}

char *read(void *apuntador, uint8_t communicationType, uint8_t id)
{
	struct netbuf *buf;
	struct netconn *conn;
	size_t n;
	uint8_t numMenu[1];
	char* data;
	if(communicationType)
	{
		conn = apuntador;
		return readSocket(conn, buf);
	}
	else
	{
		return uartR(id);
	}
	return 0;
}



