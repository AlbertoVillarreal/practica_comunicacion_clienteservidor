/*
 * lwip_freertos.h
 *
 *  Created on: 22/04/2017
 *      Author: Alberto
 */

#ifndef DRIVERS_PRACTICA1_LWIP_FREERTOS_H_
#define DRIVERS_PRACTICA1_LWIP_FREERTOS_H_

#include "lwip/opt.h"

#if LWIP_NETCONN

#include "tcpecho/tcpecho.h"
#include "lwip/tcpip.h"
#include "netif/ethernet.h"
#include "ethernetif.h"
#include "fsl_enet.h"


#include "board.h"

#include "fsl_device_registers.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "api.h"
/*******************************************************************************
 * Definitions
 ******************************************************************************/

#define EXAMPLE_ENET ENET

/* IP address configuration. */
#define configIP_ADDR0 192
#define configIP_ADDR1 168
#define configIP_ADDR2 0
#define configIP_ADDR3 204

/* Netmask configuration. */
#define configNET_MASK0 255
#define configNET_MASK1 255
#define configNET_MASK2 255
#define configNET_MASK3 0

/* Gateway address configuration. */
#define configGW_ADDR0 192
#define configGW_ADDR1 168
#define configGW_ADDR2 0
#define configGW_ADDR3 100

#define configPHY_ADDRESS 1


void ethernetInit();


#endif /* DRIVERS_PRACTICA1_LWIP_FREERTOS_H_ */
