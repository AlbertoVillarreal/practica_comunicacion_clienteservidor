/*
 * abstractionL.h
 *
 *  Created on: 13/05/2017
 *      Author: Alberto
 */

#ifndef DRIVERS_PRACTICA1_ABSTRACTIONL_H_
#define DRIVERS_PRACTICA1_ABSTRACTIONL_H_

#include "fsl_uart.h"
#include "api.h"
#include "General_functions.h"
#include "Task_Functions.h"
#include "UART.h"

/*
 * Variables para la UART
 */
#define TERMINAL_UART UART0
#define TERMINAL_UART_CLKSRC UART0_CLK_SRC
uint8_t background_buffer[32];
struct _uart_handle t_handle;

uart_rtos_config_t uart_config = {
		.baudrate = 115200,
		.parity = kUART_ParityDisabled,
		.stopbits = kUART_OneStopBit,
		.buffer = background_buffer,
		.buffer_size = sizeof(background_buffer),
};

const char *mensaje_menu1 = "1) Leer Memoria I2C\n2) Escribir memoria I2C\n3) Establecer Hora"
		"\n4) Establecer Fecha\n5) Formato de Hora\n6) Leer Hora\n7)Leer Fecha\n"
		"8) Comunicacion con otro usuario\n9) Eco en LCD\n";

const char *mensaje = "\033[2J\033[1;1H1) Leer Memoria I2C \r\n2) Escribir memoria I2C \r\n3) Establecer Hora"
		"\r\n4) Establecer Fecha\r\n5) Formato de Hora\r\n6) Leer Hora\r\n7)Leer Fecha\r\n"
		"8) Comunicacion con terminal 2\r\n9) Eco en LCD";


void write(void *apuntador, uint8_t communicationType, const char * mensaje, uint8_t id);
char *read(void *apuntador, uint8_t communicationType, uint8_t id);


#endif /* DRIVERS_PRACTICA1_ABSTRACTIONL_H_ */
