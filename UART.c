/*
 * UART.c
 *
 *  Created on: 14/05/2017
 *      Author: Alberto
 */
#include "UART.h"
/*Funciones de la UART*/
/*
 * Inicialización de la UART
 *
 *
 */

port_pin_config_t config3 = {
		kPORT_PullUp,
		kPORT_FastSlewRate,
		kPORT_PassiveFilterDisable,
		kPORT_OpenDrainEnable,
		kPORT_LowDriveStrength,
		kPORT_MuxAlt3,
		kPORT_UnlockRegister
};

static uartStruct uartStruct_type[2] =
{
		{DEMO_UART, 0},
		{UART4, 0},
};

void UART_UserCallback(UART_Type *base, uart_handle_t *handle, status_t status, void *userData)
{
	userData = userData;

	if (kStatus_UART_TxIdle == status)
	{
		txBufferFull = false;
		txOnGoing = false;
	}

	if (kStatus_UART_RxIdle == status)
	{
		rxBufferEmpty = false;
		rxOnGoing = false;
	}
}


void UART4_UserCallback(UART_Type *base, uart_handle_t *handle, status_t status, void *userData)
{
	userData = userData;

	if (kStatus_UART_TxIdle == status)
	{
		tx4BufferFull = false;
		tx4OnGoing = false;
	}

	if (kStatus_UART_RxIdle == status)
	{
		rx4BufferEmpty = false;
		rx4OnGoing = false;
	}
}

/*
 * Función para inicializar la UART que se utilizara para la terminal, en este caso la 0
 */
void terminalInit()
{
	UART_GetDefaultConfig(&config);
	config.baudRate_Bps = BOARD_DEBUG_UART_BAUDRATE;
	config.enableTx = true;
	config.enableRx = true;
	UART_Init(DEMO_UART, &config, CLOCK_GetFreq(DEMO_UART_CLKSRC));
	UART_TransferCreateHandle(DEMO_UART, &g_uartHandle, UART_UserCallback, NULL);
}

/*
 * Función para inicializar la UART que se utilizara para el bluetooth, en este caso la 4
 */
void terminalBluetoothInit()
{
	PORT_SetPinConfig(PORTC,14u,&config3);
	PORT_SetPinConfig(PORTC,15u,&config3);
	UART_GetDefaultConfig(&uart4Config);
	uart4Config.baudRate_Bps = 9600;
	uart4Config.enableTx = true;
	uart4Config.enableRx = true;
	UART_Init(UART4, &uart4Config, CLOCK_GetFreq(UART4_CLK_SRC));
	UART_TransferCreateHandle(UART4, &g_uart4Handle, UART4_UserCallback, NULL);

}

/*
 * Función para enviar datos por medio de la UART
 */
void uartSend(char * mensaje, uint8_t id)
{
	if(UART4 == uartStruct_type[id].uart)
	{
		transfer4.data = (uint8_t*) mensaje;
		transfer4.dataSize = strlen(mensaje);
		tx4OnGoing = true;
		UART_TransferSendNonBlocking(UART4, &g_uart4Handle, &transfer4);
		/* Wait send finished */
		while (tx4OnGoing)
		{
		}
	}
	else
	{
		xfer.data = (uint8_t*) mensaje;
		xfer.dataSize = strlen(mensaje);
		txOnGoing = true;
		UART_TransferSendNonBlocking(UART0, &g_uartHandle, &xfer);
		/* Wait send finished */
		while (txOnGoing)
		{
		}
	}
}

/*
 * Función pra recibir datos por medio de la UART
 */
char* uartR(uint8_t id)
{
	if(UART4 == uartStruct_type[id].uart)
	{
		uint8_t x = -1;
		sendXfer4.data = g_tx4Buffer;
		sendXfer4.dataSize = 1;
		receiveXfer4.data = g_rx4Buffer;
		receiveXfer4.dataSize = 1;
		g_rx4Buffer[BIT0] = 0;
		while (0xD != g_rx4Buffer[BIT0])
		{
			/* If RX is idle and g_rxBuffer is empty, start to read data to g_rxBuffer. */
			if ((!rx4OnGoing) && rx4BufferEmpty)
			{
				rx4OnGoing = true;
				UART_TransferReceiveNonBlocking(UART4, &g_uart4Handle, &receiveXfer4, NULL);
				recive4Buffer[x] = g_rx4Buffer[BIT0];
				x++;
			}
			/* If TX is idle and g_txBuffer is full, start to send data. */
			if ((!tx4OnGoing) && tx4BufferFull)
			{
				tx4OnGoing = true;
				UART_TransferSendNonBlocking(UART4, &g_uart4Handle, &sendXfer4);
			}

			/* If g_txBuffer is empty and g_rxBuffer is full, copy g_rxBuffer to g_txBuffer. */
			if ((!rx4BufferEmpty) && (!tx4BufferFull))
			{
				memcpy(g_tx4Buffer, g_rx4Buffer, ECHO_BUFFER_LENGTH);
				rx4BufferEmpty = true;
				tx4BufferFull = true;
			}
		}
		recive4Buffer[x] = '\r';
		return (char *)recive4Buffer;
	}
	else
	{
		uint8_t y = -1;
		sendXfer.data = g_txBuffer;
		sendXfer.dataSize = 1;
		receiveXfer.data = g_rxBuffer;
		receiveXfer.dataSize = 1;
		g_rxBuffer[BIT0] = 0;
		while (0xD != g_rxBuffer[BIT0])
		{
			/* If RX is idle and g_rxBuffer is empty, start to read data to g_rxBuffer. */
			if ((!rxOnGoing) && rxBufferEmpty)
			{
				rxOnGoing = true;
				UART_TransferReceiveNonBlocking(UART0, &g_uartHandle, &receiveXfer, NULL);
				reciveBuffer[y] = g_rxBuffer[BIT0];
				y++;
			}
			/* If TX is idle and g_txBuffer is full, start to send data. */
			if ((!txOnGoing) && txBufferFull)
			{
				txOnGoing = true;
				UART_TransferSendNonBlocking(UART0, &g_uartHandle, &sendXfer);
			}

			/* If g_txBuffer is empty and g_rxBuffer is full, copy g_rxBuffer to g_txBuffer. */
			if ((!rxBufferEmpty) && (!txBufferFull))
			{
				memcpy(g_txBuffer, g_rxBuffer, ECHO_BUFFER_LENGTH);
				rxBufferEmpty = true;
				txBufferFull = true;
			}
		}
		reciveBuffer[y] = '\r';
		return (char *)reciveBuffer;
	}
}
/*
 * Función para enviar mensajes a la otra terminal
 */
void terminalCom(uint8_t id)
{
	char * mensaje;
	uartStruct_type[id].chatFlag = pdTRUE;
	uartSend(erase, id);
	uartSend(mensajeChat, id);
	while(SALIDA != mensaje[0])
	{
		if(BIT0 == id)
		{
			if( pdTRUE == uartStruct_type[BIT1].chatFlag )/*Condicional para ver si la otra termina esta en el chat*/
			{
				mensaje = uartR(id);
				uartSend(mensaje, BIT1);
				uartSend(mensajenext, BIT1);
			}
			else
			{
				mensaje = uartR(id);
			}
		}
		else if(BIT1 == id)
		{
			if(pdTRUE == uartStruct_type[BIT0].chatFlag)/*Condicional para ver si la otra terminal esta en el chat*/
			{
				mensaje = uartR(id);
				uartSend(mensaje, BIT0);
				uartSend(mensajenext, BIT0);

			}
			else
			{
				mensaje = uartR(id);
			}
		}

	}
	uartStruct_type[id].chatFlag = pdFALSE;

}



