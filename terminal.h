/*
 * terminal.h
 *
 *  Created on: 17/03/2017
 *      Author: Alberto
 */

#ifndef SOURCE_TERMINAL_H_
#define SOURCE_TERMINAL_H_

/* FreeRTOS kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"

#include "fsl_gpio.h"
#include "board.h"

#include "fsl_uart.h"
#include "fsl_uart_freertos.h"

#include "pin_mux.h"
#include "clock_config.h"
#include "fsl_debug_console.h"
#include "fsl_device_registers.h"

#include "err.h"
#include "api.h"
#include "General_functions.h"

#include "Task_Functions.h"
#include "abstractionL.h"

stateServerDefinitionType listen(void * apuntador, uint8_t communicationType, uint8_t id);

struct netconn* clientId(uint8_t id);

void userCommunication(uint8_t id, struct netconn *conn);

void formatoHora(uint8_t hora[]);
void ecoLCD(uint8_t LCD[]);


#endif /* SOURCE_TERMINAL_H_ */
