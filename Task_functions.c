/*
 * Task_functions.c
 *
 *  Created on: 01/04/2017
 *      Author: Salvador
 */

#include "Task_Functions.h"


uart_rtos_handle_t handle;
/*Funci�n para recivir datos del socket test*/
char* readSocket(struct netconn *conn, struct netbuf *buf)
{
	void *data;
	uint16_t len;

	netconn_recv(conn, &buf); //Esta esperandoa a que le llegue un mensaje
	netbuf_data(buf, &data, &len);
	char * data2 = strcat(data,"New");
	netbuf_delete(buf);
	return data2;
}

void readMemory(struct netconn *conn, uint8_t communicationType, uint8_t id)
{
	/*Variables para leer en la memoria*/
	uint16_t address = 0;
	uint8_t i = 0;
	uint8_t bytes_number = 0;
	/*Variables para recibir infomraci�n del cliente*/
	char * data2;

	/*Escribimos el menu de lectura de memoria*/
	write(conn, communicationType, menu_readMem1, id);
	data2 = read(conn, communicationType, id);

	/*Concatenamos los datos recibidos en una sola variable para la direcci�n de
	 * memoria y enviar el par�metro a la funci�n de lectura*/
	for( i = 2; i<6; i++)
	{
		if(( (ASCII_NUMBER-1) < data2[i] ) && ( (ASCII_NUMBER + 10) > data2[i]))
			address |= (data2[i] - ASCII_NUMBER) << (4*(6-(i+1)));
		else
			address |= (data2[i] - ASCII_LETTER) << (4*(6-(i+1)));
	}

	/*Escribimos la nueva l�nea de texto para la longitud de bytes*/
	write(conn, communicationType, menu_readMem2, id);
	/*Nos esperamos hasta recibir algo del socket test*/
	data2 = read(conn, communicationType, id);


	/*Concatenamos los datos recibidos en una sola variable para la cantidad de
	 * bytes a leer y enviar el par�metro a la funci�n de lectura*/
	for(uint8_t i = 0; i<2; i++)
	{
		if(( (ASCII_NUMBER-1) < data2[i] ) && ( (ASCII_NUMBER + 10) > data2[i]))
			bytes_number |= (data2[i] - ASCII_NUMBER) << (4*(2-(i+1)));
		else
			bytes_number |= (data2[i] - ASCII_LETTER) << (4*(2-(i+1)));
	}

	/*Escribimos la nueva l�nea de texto para la comenzar a escribir el contenido*/
	write(conn, communicationType, menu_readMem3, id);

	/*Arreglo para guardar el contenido que se lee de la memoria*/
	char text[bytes_number];

	/*Comenzamos a guardar en el arreglo los bytes le�dos*/
	for(uint8_t counter = 0; counter<bytes_number; counter++)
	{
		text[counter] = EEPROM_getData(address);
		address += BYTE_LENGTH;
	}
	/*Escribimos el contenido de la memoria en la interfaz del socket test*/
	write(conn, communicationType, text, id);
	/*Esperamos la condici�n de salida para regresar al menu principal*/
	if(SALIDA != data2)
	{
		data2 = read(conn, communicationType, id);

	}

}

void writeMemory(struct netconn *conn, uint8_t communicationType, uint8_t id)
{
	/*Variables para escribir en la memoria*/
	uint16_t address = 0;
	uint8_t i = 0;

	/*Variables para recibir infomraci�n del cliente*/
	struct netbuf *buf;
	char * data2;

	/*Escribimos el menu de escritura de memoria*/
	write(conn, communicationType, menu_writeMem1, id);
	/*Nos esperamos hasta recibir algo del socket test*/
	data2 = read(conn, communicationType, id);

	/*Concatenamos los datos recibidos en una sola variable para la direcci�n de
	 * memoria y enviar el par�metro a la funci�n de lectura*/
	for( i = 2; i < 6; i++)
	{
		if(( (ASCII_NUMBER-1) < data2[i] ) && ( (ASCII_NUMBER + 10) > data2[i]))
			address |= (data2[i] - ASCII_NUMBER) << (4*(6-(i+1)));
		else
			address |= (data2[i] - ASCII_LETTER) << (4*(6-(i+1)));
	}

	/*Escribimos la nueva l�nea de texto para la comenzar a escribir el contenido*/
	write(conn, communicationType, menu_writeMem2, id);
	/*Nos esperamos hasta recibir algo del socket test*/
	data2 = read(conn, communicationType, id);

	/*Iniciamos un contador en 0 para comenzar a escribir los datos que nos llegaron de la
	 * interfaz del socket test a la memoria*/
	i = 0;
	while(FINAL != data2[i])
	{
		EEPROM_writeData(address, data2[i]);
		address += BYTE_LENGTH;
		i++;
	}

	/*Escribimos la nueva l�nea de texto para decir que la memoria se escribi� correctamente*/
	write(conn, communicationType, menu_writeMem3, id);


	/*Esperamos la condici�n de salida para regresar al menu principal*/
	if(SALIDA != data2)
	{
		data2 = read(conn, communicationType, id);
	}
}

void setUpHour(struct netconn *conn, uint8_t communicationType, uint8_t id)
{
	/*Variables para recibir informaci�n del cliente*/
	struct netbuf *buf;

	char *data2;

	/*Escribimos la nueva l�nea de texto para la modificar la hora*/
	write(conn, communicationType, menu_setHour, id);
	/*Nos esperamos hasta recibir algo del socket test*/
	data2 = read(conn, communicationType, id);


	/*Los datos que recibimos del socket test los pasamos a las variables para modificar
	 * la hora del RTC*/
	hours = ((data2[0]-ASCII_NUMBER)<<4)|(data2[1]-ASCII_NUMBER);
	minutes = ((data2[3]-ASCII_NUMBER)<<4)|(data2[4]-ASCII_NUMBER);
	seconds = ((data2[6]-ASCII_NUMBER)<<4)|(data2[7]-ASCII_NUMBER);

	/*Escribimos la nueva hora en el RTC*/
	RTC_setHours(hours);
	RTC_setMinutes(minutes);
	RTC_setSeconds(seconds);

	/*Escribimos la nueva l�nea de texto para especificar que la hora se ha modificado*/
	write(conn, communicationType, completed_setHour, id);

	/*Esperamos la condici�n de salida para regresar al menu principal*/
	if(SALIDA != data2)
	{
		data2 = read(conn, communicationType, id);

	}
}

void setUpDate(struct netconn *conn, uint8_t communicationType, uint8_t id)
{
	/*Variables para recibir informaci�n del cliente*/
	struct netbuf *buf;
	char * data2;

	/*Escribimos la nueva l�nea de texto para la modificar la hora*/
	write(conn, communicationType, menu_setDate, id);
	/*Nos esperamos hasta recibir algo del socket test*/
	data2 = read(conn, communicationType, id);


	/*Los datos que recibimos del socket test los pasamos a las variables para modificar
	 * la fecha del RTC*/
	day = ((data2[0]-ASCII_NUMBER)<<4)|(data2[1]-ASCII_NUMBER);
	month = ((data2[3]-ASCII_NUMBER)<<4)|(data2[4]-ASCII_NUMBER);
	year = ((data2[6]-ASCII_NUMBER)<<4)|(data2[7]-ASCII_NUMBER);

	/*Escribimos la nueva fecha en el RTC*/
	RTC_setDays(day);
	RTC_setMonths(month);
	RTC_setYears(year);

	/*Escribimos la nueva l�nea de texto para especificar que la fecha se ha modificado*/
	write(conn, communicationType, completed_setDate, id);

	/*Esperamos la condici�n de salida para regresar al menu principal*/
	if(SALIDA != data2)
	{
		data2 = read(conn, communicationType, id);

	}
}

void readHour(struct netconn *conn, uint8_t communicationType, uint8_t id)
{
	/*Variables para recibir informaci�n del cliente*/
	struct netbuf *buf;

	uint8_t hora_actual [BYTE_LENGTH];
	char * data2;

	/*Leemos de la RTC los registros de horas, minutos y segundos*/
	seconds = RTC_getSeconds();
	minutes = RTC_getMinutes();
	hours = RTC_getHours();

	/*Escribimos la nueva l�nea de texto para la lectura de la RTC*/
	write(conn, communicationType, menu_getHour, id);

	/*Escribimos en el arreglo los datos de la hora actual para despu�s desplegarlo en la
	 * interfaz*/
	hora_actual[0] = ((hours & HIGH_BYTE)>>4)+ASCII_NUMBER;
	hora_actual[1] = (hours & LOW_BYTE)+ASCII_NUMBER;
	hora_actual[2] = ':';
	hora_actual[3] = ((minutes & HIGH_BYTE)>>4)+ASCII_NUMBER;
	hora_actual[4] = (minutes & LOW_BYTE)+ASCII_NUMBER;
	hora_actual[5] = ':';
	hora_actual[6] = ((seconds & 0x70)>>4)+ASCII_NUMBER;
	hora_actual[7] = (seconds & LOW_BYTE)+ASCII_NUMBER;

	/*Escribimos en la terminal el arreglo*/
	write(conn, communicationType, hora_actual, id);


	/*Esperamos la condici�n de salida para regresar al menu principal*/
	if(SALIDA != data2)
	{
		data2 = read(conn, communicationType, id);
	}
}

void readDate(struct netconn *conn, uint8_t communicationType, uint8_t id)
{
	/*Variables para recibir informaci�n del cliente*/
	char fecha_actual [BYTE_LENGTH];
	struct netbuf *buf;

	char *data2;

	/*Leemos de la RTC los registros de horas, minutos y segundos*/
	day = RTC_getDays();
	month = RTC_getMonths();
	year = RTC_getYears();

	/*Escribimos la nueva l�nea de texto para la lectura de la RTC*/
	write(conn, communicationType, menu_getDate, id);

	/*Escribimos en el arreglo los datos de la fecha actual para despu�s desplegarlo en la
	 * interfaz*/
	fecha_actual[0] = ((day & HIGH_BYTE)>>4)+ASCII_NUMBER;
	fecha_actual[1] = (day & LOW_BYTE)+ASCII_NUMBER;
	fecha_actual[2] = '/';
	fecha_actual[3] = ((month & 0x10)>>4)+ASCII_NUMBER;
	fecha_actual[4] = (month & LOW_BYTE)+ASCII_NUMBER;
	fecha_actual[5] = '/';
	fecha_actual[6] = ((year & HIGH_BYTE)>>4)+ASCII_NUMBER;
	fecha_actual[7] = (year & LOW_BYTE)+ASCII_NUMBER;

	/*Escribimos en la terminal el arreglo*/
	write(conn, communicationType, fecha_actual, id);

	//netconn_write(conn, fecha_actual, BYTE_LENGTH, BIT0);

	/*Esperamos la condici�n de salida para regresar al menu principal*/
	if(SALIDA != data2)
	{
		data2 = read(conn, communicationType, id);

	}
}


void echoLCD(struct netconn *conn, uint8_t communicationType, uint8_t id)
{
	/*Variables para recibir informaci�n del cliente*/
	struct netbuf *buf;;
	uint8_t i;
	char* data2;

	/*Limpiamos la pantalla LCD*/
	LCDNokia_clear();

	write(conn, communicationType, menu_echoLCD, id);
	/*Nos esperamos hasta recibir algo del socket test*/
	data2 = read(conn, communicationType, id);


	/*Colocamos el puntero en la primera posici�n de la LCD*/
	LCDNokia_gotoXY(0,0);

	/*Escribimos en la LCD lo que recibamos del cliente*/
	i = 0;
	while(FINAL != data2[i])
	{
		LCDNokia_sendChar(data2[i]);
		i++;
	}

	/*Esperamos la condici�n de salida para regresar al menu principal*/
	if(SALIDA != data2)
	{
		data2 = read(conn, communicationType, id);

		/*Seguimos escribiendo hasta encontrar la condici�n que nos retorne al menu principal*/
		LCDNokia_clear();
		LCDNokia_gotoXY(0,0);
		i = 0;
		while(FINAL != data2[i])
		{
			LCDNokia_sendChar(data2[i]);
			i++;
		}
	}

}


void formatHours(struct netconn *conn, uint8_t communicationType, uint8_t id)
{
	/*Variables para recibir informaci�n del cliente*/
	struct netbuf *buf;
	char * data2;

	/*Obtenemos el bit del formato del RTC*/
	formatHour = FORMATBIT1 & RTC_getHours();

	/*Verificamos si nos encontramos en formato de 12 horas*/
	if(FORMATBIT1 == formatHour){

		/*Escribimos las l�neas de texto correspondientes a este menu*/
		write(conn, communicationType, menu_formatoHora12_1, id);
		write(conn, communicationType, menu_formatoHora12_1, id);

		data2 = read(conn, communicationType, id);


		/**/
		if(YES == data2[BIT0])
		{
			am_pm = AMPMBIT & RTC_getHours();
			if(am_pm)
			{
				hours = ((FORMATMASK) & RTC_getHours()) + MAX_HOUR;
			}
			else
			{
				hours = ((FORMATMASK) & RTC_getHours());
			}
			RTC_setHours(hours);
			write(conn, communicationType, completed_formatoHora, id);

			if(SALIDA != data2)
			{
				data2 = read(conn, communicationType, id);


			}
		}
		else;
	}
	else
	{
		write(conn, communicationType, menu_formatoHora24_2, id);
		write(conn, communicationType, menu_formatoHora24_1, id);

		data2 = read(conn, communicationType, id);

		if(YES == data2[BIT0])
		{
			if(OVERFLOW_HOUR <= RTC_getHours())
			{
				hours = AMPMBIT | (RTC_getHours() - MAX_HOUR);
			}
			else
			{
				hours = (~AMPMBIT) & (RTC_getHours());
			}
			hours |= FORMATBIT1;
			RTC_setHours(hours);
			write(conn, communicationType, completed_formatoHora, id);
			if(SALIDA != data2)
			{
				data2 = read(conn, communicationType, id);

			}
		}
		else;
	}
}

void LCD_Print()
{
	uint8_t hora_actual [BYTE_LENGTH];
	uint8_t fecha_actual [BYTE_LENGTH];

	day = RTC_getDays();
	month = RTC_getMonths();
	year = RTC_getYears();
	seconds = RTC_getSeconds();
	minutes = RTC_getMinutes();
	formatHour = FORMATBIT1 & RTC_getHours();

	LCDNokia_clear();

	if(formatHour)
	{
		hours = FORMATMASK & RTC_getHours();
		if(OVERFLOW_HOUR <= hours)
		{
			hours = AMPMBIT|(hours-MAX_HOUR);
		}
		else
		{
			hours &= (~AMPMBIT);
		}
	}
	else
	{
		hours = RTC_getHours();
	}

	if(TRUE == GPIO_getFlagPortCX(BIT0)){
		day++;
		if((day & LOW_BYTE) == 0x0A)
			day += 0x10;
		GPIO_setFlagPortCX(BIT0, FALSE);
	}
	if(TRUE == GPIO_getFlagPortCX(BIT1)){
		month++;
		if((month & LOW_BYTE) == 0x0A)
			month += 0x10;
		GPIO_setFlagPortCX(BIT1, FALSE);
	}
	if(TRUE == GPIO_getFlagPortCX(BIT7)){
		year++;
		if((year & LOW_BYTE) == 0x0A)
			year += 0x10;
		GPIO_setFlagPortCX(BIT7, FALSE);
	}
	if(TRUE == GPIO_getFlagPortCX(BIT3)){
		hours++;
		GPIO_setFlagPortCX(BIT3, FALSE);
	}
	if(TRUE == GPIO_getFlagPortCX(BIT4)){
		minutes++;
		if((minutes & LOW_BYTE) == 0x0A)
			minutes += 0x10;
		GPIO_setFlagPortCX(BIT4, FALSE);
	}
	if(TRUE == GPIO_getFlagPortCX(BIT5)){
		seconds++;
		if((seconds & LOW_BYTE) == 0x0A)
			seconds += 0x10;
		GPIO_setFlagPortCX(BIT5, FALSE);
	}

	RTC_setYears(year);
	RTC_setMonths(month);
	RTC_setDays(day);
	RTC_setHours(hours);
	RTC_setMinutes(minutes);
	RTC_setSeconds(seconds);

	day = RTC_getDays();
	month = RTC_getMonths();
	year = RTC_getYears();
	seconds = RTC_getSeconds();
	minutes = RTC_getMinutes();
	formatHour = FORMATBIT1 & RTC_getHours();

	LCDNokia_clear();

	if(formatHour)
	{
		hours = FORMATMASK & RTC_getHours();
		if(OVERFLOW_HOUR <= hours)
		{
			hours = AMPMBIT|(hours-MAX_HOUR);
		}
		else
		{
			hours &= (~AMPMBIT);
		}
	}
	else
	{
		hours = RTC_getHours();
	}

	fecha_actual[0] = ((day & HIGH_BYTE)>>4)+ASCII_NUMBER;
	fecha_actual[1] = (day & LOW_BYTE)+ASCII_NUMBER;
	fecha_actual[2] = '/';
	fecha_actual[3] = ((month & 0x10)>>4)+ASCII_NUMBER;
	fecha_actual[4] = (month & LOW_BYTE)+ASCII_NUMBER;
	fecha_actual[5] = '/';
	fecha_actual[6] = ((year & HIGH_BYTE)>>4)+ASCII_NUMBER;
	fecha_actual[7] = (year & LOW_BYTE)+ASCII_NUMBER;

	hora_actual[0] = ((hours & HIGH_BYTE)>>4)+ASCII_NUMBER;
	hora_actual[1] = (hours & LOW_BYTE)+ASCII_NUMBER;
	hora_actual[2] = ':';
	hora_actual[3] = ((minutes & HIGH_BYTE)>>4)+ASCII_NUMBER;
	hora_actual[4] = (minutes & LOW_BYTE)+ASCII_NUMBER;
	hora_actual[5] = ':';
	hora_actual[6] = ((seconds & 0x70)>>4)+ASCII_NUMBER;
	hora_actual[7] = (seconds & LOW_BYTE)+ASCII_NUMBER;

	LCDNokia_gotoXY(0,0);
	LCDNokia_sendString(string1_LCD);
	LCDNokia_gotoXY(5,1);

	uint8_t array_length = sizeof(fecha_actual)/sizeof(fecha_actual[0]);

	for(uint8_t i = 0 ; i < array_length ; i++)
	{
		LCDNokia_sendChar(fecha_actual[i]);
	}

	LCDNokia_gotoXY(0,2);
	LCDNokia_sendString(string2_LCD);
	LCDNokia_gotoXY(5,3);

	array_length = sizeof(hora_actual)/sizeof(hora_actual[0]);

	for(uint8_t i = 0 ; i < array_length ; i++)
	{
		LCDNokia_sendChar(hora_actual[i]);
	}
}



